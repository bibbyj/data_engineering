
# Stage 1
FROM python:3.10.11-slim-buster as base

WORKDIR /app
COPY docker/requirements.txt .

RUN pip install -r requirements.txt

# Stage 2
FROM base
WORKDIR /app

COPY docker/test-app.py .
COPY deploy/* deploy/
COPY src/* src/
COPY weather.db .

EXPOSE 80

CMD ["python", "test-app.py"]
