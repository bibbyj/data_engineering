import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    '''
    create a database connection to the SQLite database
    '''
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn


def create_table(conn, create_table_sql):
    '''
    create a table from the create_table_sql statement
    '''
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def create_weather_report(conn, weather_info):
    '''
    create a new report in the weather table
    '''
    sql = '''
        INSERT INTO weather(tod,location,temperature,windspeed,conditions, rain)
        VALUES(?,?,?,?,?,?)
    '''
    cur = conn.cursor()
    cur.execute(sql, weather_info)
    conn.commit()
    return cur.lastrowid


def get_weather_reports(conn, get_reports_sql):
    '''
    get weather reports from the weather table
    '''
    cursor = conn.cursor()
    cursor.execute(get_reports_sql)
    rows = cursor.fetchall()
    return rows


def get_latest_weather(conn):
    SQL_LATEST_WEATHER = '''
        select *
        from weather
        where id = (select max(id) from weather)
    '''
    reports = get_weather_reports(conn, SQL_LATEST_WEATHER)
    return reports[0]


if __name__ == "__main__":
    DATABASE = r"weather.db"
    conn = sqlite3.connect(DATABASE)
    cursor = conn.cursor()
    cursor.execute("select * from weather group by tod")
    rows = cursor.fetchall()
    for row in rows:
        print(row)

