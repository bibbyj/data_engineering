
def print_dict(dict):
    '''
    quick print a dictionary
    '''
    for key, value in dict.items():
            print(f"{key}: {value}")