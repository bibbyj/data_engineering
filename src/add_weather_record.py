
from api.openweather.apikey import API_KEY
from api.openweather.weather_functions import get_weather
from db.database_functions import create_connection, create_weather_report

CITY = "London,GB-SWK"
API_URL = f'http://api.openweathermap.org/data/2.5/weather?q={CITY}&appid={API_KEY}&units=metric'

weather_dict = get_weather(url=API_URL, json_file="query_log.json")

WEATHER_DB = "weather.db"
conn = create_connection(WEATHER_DB)
with conn:
    weather = [value for key,value in weather_dict.items()]
    weather_id = create_weather_report(conn, weather)