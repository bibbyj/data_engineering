
from db.database_functions import create_connection, get_latest_weather
from api.openweather.weather_functions import decode_weather_record

from generic.generic_functions import print_dict

WEATHER_DB = "weather.db"

conn = create_connection(WEATHER_DB)
lw = get_latest_weather(conn)
lw_dict = decode_weather_record(lw)
print_dict(lw_dict)