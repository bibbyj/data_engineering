
def get_icons(weather_dict):

    if weather_dict["temperature"] > 18.0:
        icon_a = "day_clear.png"
    elif weather_dict["temperature"] > 12.0:
        icon_a = "day_partial_cloud.png"
    elif weather_dict["temperature"] > 8.0:
        icon_a = "overcast.png"
    elif weather_dict["temperature"] < 0.0:
        icon_a = "snow.png"
    else:
        icon_a = "day_sleet.png"

    if weather_dict["rain"] == None:
        icon_b = "day_clear.png"
    elif weather_dict["rain"] > 2.0:
        icon_b = "rain.png"
    elif weather_dict["rain"] > 0.5:
        icon_b = "day_rain.png"
    else:
        icon_b = "day_clear.png"

    return  "weather_icons/"+icon_a, "weather_icons/"+icon_b