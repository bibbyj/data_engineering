
import requests
import json


def query_weather_api(url):
    '''
    submit the API query using API_KEY
    '''
    try:
        data = requests.get(url).json()
    except Exception as exc:
        print(url)
        print(exc)
        data = None
    return data


def dump_weather_report(weather_api_resp, file):
    '''
    write a nicely formatted json dump to file
    '''
    with open(file, 'w') as file:
            json.dump(weather_api_resp,
                      file,
                      sort_keys = True,
                      indent = 4,
                      ensure_ascii = False)


def extract_rain_report(weather_api_resp):
    try:
        rain = weather_api_resp["rain"]["1h"]
    except KeyError:
        rain = None
    return rain


def format_weather_report(weather_api_resp):
    info = {
        # always in the report
        "tod": weather_api_resp["dt"],
        "location": weather_api_resp["name"],
        "temperature": weather_api_resp["main"]["temp"],
        "windspeed": weather_api_resp["wind"]["speed"],
        "conditions": weather_api_resp["weather"][0]["description"],

        # rain doesn't appear unless applicable
        "rain": extract_rain_report(weather_api_resp)
    }
    return info


def get_weather(url, json_file=None):
    resp = query_weather_api(url)
    valid_resp = (resp["cod"] == 200)

    if valid_resp:
        if json_file is not None:
            dump_weather_report(resp, json_file)
        info = format_weather_report(resp)
        return info
    else:
        info = {
            "HTTP Code": resp["cod"],
            "Message": resp["message"]
        }
        return None


def decode_weather_record(db_weather_record):
    '''
    decodes weather records queried from the db weather table
    '''
    info = {
        "tod": db_weather_record[1],
        "location": db_weather_record[2],
        "temperature": db_weather_record[3],
        "windspeed": db_weather_record[4],
        "conditions": db_weather_record[5],
        "rain": db_weather_record[6]
    }
    return info
