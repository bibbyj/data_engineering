
from flask import Flask, render_template, url_for
app = Flask(__name__)

import datetime

import sys
from pathlib import Path
ROOT = Path(__file__).parent.parent
sys.path.append(f"{ROOT}")

from src.db.database_functions import create_connection, get_latest_weather
from src.api.openweather.weather_functions import decode_weather_record
from src.webpage.webpage_functions import get_icons


@app.route('/')
def hello():
    return 'Hello World!'

@app.route("/user/<name>")
def user(name):
    T_name = name.title()
    return f"Hello {T_name}"

@app.route("/niceuser/<name>")
def test_template(name):
    return render_template("hello.html", name=name)

@app.route("/weather")
def latest_weather():
    DATABASE = r"weather.db"
    conn = create_connection(DATABASE)
    weather_dict = (decode_weather_record(get_latest_weather(conn)))

    return render_template(
        "fancy_weather.html",
        time = datetime.datetime.fromtimestamp(weather_dict["tod"]).strftime('%c'),
        location = weather_dict["location"],
        temperature = weather_dict["temperature"],
        windspeed = weather_dict["windspeed"],
        conditions = weather_dict["conditions"],
        rain = weather_dict["rain"],
        icon_a = url_for('static', filename=get_icons(weather_dict)[0]),
        icon_b = url_for('static', filename=get_icons(weather_dict)[1]),
        )


if __name__ == '__main__':
   app.run(debug = True)
